# Create your views here.
from django.http import HttpResponse, response
from django.shortcuts import get_object_or_404, render

from .models import Account
from twitter_app import settings
import requests


def index(request):
    popular_account = Account.objects.order_by('count')[:5]
    context = {
        'popular_account': popular_account
    }
    return render(request, 'collect_tweets/index.html', context)


def search(request, handle):
    search_result = get_object_or_404(Account, pk=handle)
    return render(request, 'collect_tweets/search.html', {'search': search_result})


def monitor_handle(request):
    return render(request, 'collect_tweets/monitor.html')


def delete(request):
    return render(request, 'collect_tweets/delete.html')


def delete_account(request):
    handle = request.POST['handle']
    account = Account.objects.get(pk=handle)
    if account:
        account.delete()
    return render(request, 'collect_tweets/delete_success.html', {'handle': handle})


def register(request):
    handle = request.POST['handle']
    url = f"https://api.twitter.com/2/users/by/username/{handle[1:]}?expansions=pinned_tweet_id&user.fields=created_at&tweet.fields=public_metrics"
    response = requests.request("GET",url, headers={
        "Authorization": f"Bearer {settings.TWITTER_BEARER_TOKEN}"
    })
    response = response.json()
    data = response['data']
    id = data.get('id',0)
    count = 0
    try:
        public_metrics = response['includes']['tweets'][0].get('public_metrics',{})
        if public_metrics:
            for metric in public_metrics:
                count += public_metrics[metric]
    except:
        pass
    account = Account()
    account.handle = handle
    account.id = id
    account.count = count
    account.save()
    return render(request, 'collect_tweets/register.html', {'account': account})
