from django.urls import path

from . import views
app_name = 'collect_tweets'
urlpatterns = [
    path('', views.index, name='index'),
    path('search/<str:handle>', views.search, name='search'),
    path('monitor', views.monitor_handle, name='monitor'),
    path('register',views.register,name="register"),
    path('delete',views.delete,name="delete"),
    path('delete_account',views.delete_account,name="delete_account")
]
