from django.apps import AppConfig


class CollectTweetsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'collect_tweets'
